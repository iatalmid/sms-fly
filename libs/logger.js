const bunyan = require('bunyan');
const PrettyStream = require('bunyan-prettystream');
const path = require('path');
const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);
/**
 * @namespace
 * @property trace
 * @property debug
 * @property info
 * @property warning
 * @property error
 * @property fatal
 * @property child
 * @property level
 */
module.exports = function Logger(logDir) {
  const streams = [];
  if(process.env.NODE_ENV === 'production') {
    streams.push({
      level: 'info',
      path: logDir || '',
      type: 'rotating-file',
      period: '1d',   // daily rotation
      count: 30        // keep 3 back copies
    });

  } else {
    streams.push({
      level: 'trace',
      type: 'raw',
      stream: prettyStdOut
    });
  }
  const logger = bunyan.createLogger({
    name: 'sms',
    streams: streams,
    src: true
  });
  return logger;
}



