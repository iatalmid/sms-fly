const rp = require('request-promise-native'),
  jsonxml = require('jsontoxml'),
  Logger = require('./logger'),
  {parseString} = require('xml2js');
let logger;

module.exports = class SMSFly{
  constructor(login, password, url) {
    logger = new Logger();
    if(typeof(login) === 'string') {
      this.login = login;
    }
    if(typeof(password) === 'string')  {
      this.password = password;
    }
    this.url = url || 'http://sms-fly.com/api/api.php';
  }

  async sendMessage(message, recipient, options) {
    if(message.length === 0){
      return Promise.resolve();
    }
    logger.info(`message to ${recipient}: ${message}`);
    options = options || {};
    options.start_time = options.start_time || 'AUTO';
    options.end_time = options.end_time || 'AUTO';
    options.lifetime = options.lifetime || 24;
    options.rate = options.rate || 120;
    options.desc = options.desc || '';
    options.source = options.source || 'INFO';

    let recipientObj;
    if(typeof(recipient) === 'string') {
      recipientObj = {recipient};
    } else if(Array.isArray(recipient)) {
      recipientObj = recipient.map(item => ({recipient: item}));
    }

    const json = {
      request: [
        { operation: 'SENDSMS'},
        {
          name: 'message',
          attrs: {
            start_time: options.start_time,
            end_time: options.end_time,
            lifetime: options.lifetime,
            rate: options.rate,
            desc: options.desc,
            source: options.source
          },
          children: [
            {body: message},
            recipientObj
          ]
        }
      ]
    };
    logger.trace(json);
    const xml = this.getXML(json);
    logger.trace(xml);

    const result = await this.sendRequest(xml);
    logger.debug(result);
    return new Promise((resolve, reject) => {
      parseString(result, function (err, result) {
        if(err) {
          logger.error(err);
          return reject(err);
        }
        logger.debug(result);
        return resolve(result);
      });
    });
  }

  sendRequest(payload) {
    const auth = new Buffer.from(`${this.login}:${this.password}`).toString("base64");
    const options = {
      method: 'POST',
      uri: this.url,
      body: payload,
      headers: {
        Authorization: `Basic ${auth}`
      }
    };
    logger.trace(options);
    return rp(options);
  }

  getXML(json) {
    return jsonxml(json, {xmlHeader: true, prettyPrint: true});
  }
}

