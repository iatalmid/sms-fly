Allows to send SMS using API for service https://sms-fly.ua

example for use 

const SMSFly = require('sms-fly');

const sms = new SMSFly('username', 'password'[, url, debug]);

sms.sendMessage(message, recipient[, options])
.then(result => console.log(result));
